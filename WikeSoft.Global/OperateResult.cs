﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Global
{
    /// <summary>
    /// 操作结果
    /// </summary>
    public class OperateResult
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Success { get; set; }
        /// <summary>
        /// 错误内容
        /// </summary>
        public String Message { get; set; }
        /// <summary>
        /// 构造方法
        /// </summary>
        public OperateResult()
        {
           
            Success = true;
        }
    }
}
