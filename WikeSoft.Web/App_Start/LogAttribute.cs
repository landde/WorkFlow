﻿using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

using WikeSoft.Data.Models;

namespace WikeSoft.Web
{
    /// <summary>
    /// 
    /// </summary>
    public class LogAttribute: ActionFilterAttribute
    {
        private readonly string _perationText;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationText"></param>
        public LogAttribute(string operationText)
        {
            _perationText = operationText;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            
            base.OnActionExecuting(filterContext);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
           
            base.OnActionExecuted(filterContext);
        }
    }
}