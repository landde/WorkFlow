﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using log4net;
using log4net.Config;
using WikeSoft.Web;

namespace Star.Web
{
    /// <summary>
    /// 
    /// </summary>
    public class MvcApplication : System.Web.HttpApplication
    {
        private static readonly ILog Log = LogManager.GetLogger("SystemError");

        /// <summary>
        /// 
        /// </summary>
        protected void Application_Start()
        {


            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //ioc config
            IocConfig.Register();

            //log4net
            var logFilePath = $"{AppDomain.CurrentDomain.BaseDirectory}/log4net.config";
            XmlConfigurator.ConfigureAndWatch(new FileInfo(logFilePath));

            //unhandled exception
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;
        }

        /// <summary>
        /// UnhandledExceptionEvent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = e.ExceptionObject as Exception;
            if (ex != null)
            {
                Log.Fatal("未处理的全局异常", ex);
            }
        }

        /// <summary>
        /// UnobservedTaskExceptionEvent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            var innerExceptions = e.Exception.InnerExceptions;

            foreach (var innerException in innerExceptions)
            {
                Log.Fatal("未处理的全局Task异常", innerException);
            }

            e.SetObserved();
        }
    }
}
