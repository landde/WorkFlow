﻿using System;
using System.Web.Mvc;

using WikeSoft.Data.Models.Sys;
using WikeSoft.Enterprise.Interfaces.Sys;
using WikeSoft.Enterprise.Models.Filters.Sys;

namespace WikeSoft.Web.Controllers.Sys
{
    /// <summary>
    /// KeyValue
    /// </summary>
    public class KeyValueController : BaseController
    {
        private readonly IKeyValueService _keyValueService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyValueService"></param>
        public KeyValueController(IKeyValueService keyValueService)
        {
            _keyValueService = keyValueService;
           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>

        // GET: KeyValue
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Add(KeyValueAddModel model)
        {
            if (ModelState.IsValid)
            {
                var success = _keyValueService.Add(model);
                if (success)
                    return RedirectToAction("Index");
            }
            return View(model);
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <returns></returns>
        public ActionResult Edit(Guid id)
        {
            var model = _keyValueService.Find(id);
            return View(model);
        }

     
 
 
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(KeyValueEditModel model)
        {
            if (ModelState.IsValid)
            {
                var success = _keyValueService.Edit(model);
                if (success)
                    return RedirectToAction("Index");
            }
            return View(model);
        }
 
 
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetList(KeyValueFilter filter)
        {
           
            var result = _keyValueService.Query(filter);
            return JsonOk(result);
        }

      
    }
}