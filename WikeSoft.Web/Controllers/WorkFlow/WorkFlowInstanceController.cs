﻿using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Web.Mvc;
using WikeSoft.WorkFlowEngine.Enum;
using WikeSoft.WorkFlowEngine.Filter;
using WikeSoft.WorkFlowEngine.Interfaces;
using WikeSoft.WorkFlowEngine.Models;

namespace WikeSoft.Web.Controllers.WorkFlow
{
    /// <summary>
    /// 
    /// </summary>
    public class WorkFlowInstanceController : BaseController
    {
        private IWorkFlowInstanceService _workFlowInstanceService;
        private readonly IWorkFlowInstanceService _flowInstanceService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="workFlowInstanceService"></param>
        /// <param name="flowInstanceService"></param>
        public WorkFlowInstanceController(IWorkFlowInstanceService workFlowInstanceService, IWorkFlowInstanceService flowInstanceService)
        {
            _workFlowInstanceService = workFlowInstanceService;
            _flowInstanceService = flowInstanceService;
        }

        // GET: WorkFlowInstance
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult HistoryInstanceView()
        {
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>

        [HttpGet]
        public JsonResult Query(WorkFlowInstanceFilter filter)
        {
            var result = _workFlowInstanceService.Query(filter);
            return JsonOk(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult QueryHistory(WorkFlowInstanceFilter filter)
        {
            filter.AssociatedUserId = User.Identity.GetLoginUserId().ToString().ToUpper();
            var result = _workFlowInstanceService.Query(filter);
            return JsonOk(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        public ActionResult GetTaskView( string instanceId)
        {

            List<TaskInstance> flows = _flowInstanceService.GetHistoryFlowInstances(instanceId);
            ViewBag.Historys = flows;
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult WaitToDoList()
        {
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ActionResult QueryWaitToDoList(WorkFlowInstanceFilter filter)
        {
            filter.AssociatedUserId = User.Identity.GetLoginUserId().ToString().ToUpper();
            filter.FlowRunStatus = FlowRunStatus.Run;
            
            var result = _workFlowInstanceService.Query(filter);
            return JsonOk(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="flowId"></param>
        public void Pic(string flowId)
        {
            var image = _flowInstanceService.GetRunBitmap(flowId);
            MemoryStream stream = new MemoryStream();
            image.Save(stream, ImageFormat.Jpeg);
            HttpContext.Response.Clear();
            HttpContext.Response.ContentType = "image/jpeg";
            HttpContext.Response.BinaryWrite(stream.ToArray());

        }
    }
}