﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.WorkFlowEngine.Models
{
    /// <summary>
    /// ztree页面模型
    /// </summary>
    public class ZTreeEntity
    {
        /// <summary>
        /// Id
        /// </summary>
       
        public string id { get; set; }

        /// <summary>
        /// 父节点Id
        /// </summary>
        public string pId { get; set; }

        /// <summary>
        /// 页面名称
        /// </summary>
       
        public string name { get; set; }

        /// <summary>
        ///  是否是父节点
        /// </summary>
      
        public bool isParent { get; set; }

        /// <summary>
        /// 是否展开
        /// </summary>
        public bool open { get; set; }
    }
}
