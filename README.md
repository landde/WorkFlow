WorkFlow —— .NET 流程处理

WikeFlow官网：http://www.wikesoft.com

WikeFlow演示地址：http://workflow.wikesoft.com

WikeFlow2.0 演示地址：http://workflow2.wikesoft.com

WikeFlow2.0帮助文档：http://wikeflowhelp.wikesoft.com

WikeFlow2.0 主要功能
1、流程审批，一般的业务审批

2、流程会签，一个任务节点多人审批

3、流程驳回，流程不同意

4、流程转办，流程从一个人转交给另外的人处理

5、流程传阅，将流程传递给其他的人查阅

6、流程撤回，提交后可以撤回

7、流程附件，支持节点附件

8、流程审批中修改业务数据

9、流程审批中业务数据权限设置

多数据库支持
同时支持：SQLServer，Mysql，Oracle

初衷
微软官方的WorkFlow入门要求比较高，所以我们萌生了开发一个简单的工作流引擎，帮助.Net coder们解决软件项目中流程的处理。 WikeFlow除了正常的流程处理，还支持流程动态跳转!

理念
写最少的代码，实现最炫酷的功能。

零难度
WikeFlow工作流引擎只有两个核心类,FlowDesignService(流程设计)，FlowInstanceService（流程实例）


