

CREATE TABLE [dbo].[Sys_KeyValue](
	[Id] [nvarchar](50) NOT NULL,
	[CnName] [nvarchar](500) NULL,
	[Key] [nvarchar](500) NULL,
	[Value] [nvarchar](500) NULL,
	[Memo] [nvarchar](500) NULL,
 CONSTRAINT [PK_dbo.Sys_KeyValue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_KeyValue] ([Id], [CnName], [Key], [Value], [Memo]) VALUES (N'46AEFF2D-5DD4-4470-97B6-64757042EC59', N'系统名称', N'SystemTitle', N'WikeFlow', N'系统名称')
/****** Object:  Table [dbo].[Sys_Department]    Script Date: 08/02/2018 15:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_Department](
	[Id] [nvarchar](50) NOT NULL,
	[DepartmentName] [nvarchar](500) NULL,
	[DepartmentCode] [nvarchar](500) NULL,
	[ParentId] [nvarchar](50) NULL,
	[Remark] [nvarchar](500) NULL,
	[PathCode] [nvarchar](500) NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Sys_Department] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_Department] ([Id], [DepartmentName], [DepartmentCode], [ParentId], [Remark], [PathCode], [IsDelete]) VALUES (N'20CE56AA-0645-41FD-9FF6-51364D0A62F7', N'行政部', NULL, N'884FC7B8-47A1-44FD-8970-4FF16A655E87', NULL, N'AAAAAB', 0)
INSERT [dbo].[Sys_Department] ([Id], [DepartmentName], [DepartmentCode], [ParentId], [Remark], [PathCode], [IsDelete]) VALUES (N'4A7335F8-367E-4C06-82C0-4DB694DEC5E2', N'Wikesoft', NULL, NULL, NULL, N'AA', 0)
INSERT [dbo].[Sys_Department] ([Id], [DepartmentName], [DepartmentCode], [ParentId], [Remark], [PathCode], [IsDelete]) VALUES (N'59485231-7B38-4A95-BCDA-721F87A5BFBB', N'研发部', NULL, N'884FC7B8-47A1-44FD-8970-4FF16A655E87', NULL, N'AAAAAA', 0)
INSERT [dbo].[Sys_Department] ([Id], [DepartmentName], [DepartmentCode], [ParentId], [Remark], [PathCode], [IsDelete]) VALUES (N'884FC7B8-47A1-44FD-8970-4FF16A655E87', N'总经理办公室', NULL, N'4A7335F8-367E-4C06-82C0-4DB694DEC5E2', NULL, N'AAAA', 0)
INSERT [dbo].[Sys_Department] ([Id], [DepartmentName], [DepartmentCode], [ParentId], [Remark], [PathCode], [IsDelete]) VALUES (N'B0DF6F62-7068-4FA6-82B2-597494327937', N'研发一部', NULL, N'59485231-7B38-4A95-BCDA-721F87A5BFBB', NULL, N'AAAAAAAA', 0)
/****** Object:  Table [dbo].[Sys_DataDictionay]    Script Date: 08/02/2018 15:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_DataDictionay](
	[Id] [nvarchar](50) NOT NULL,
	[DictionayName] [nvarchar](100) NULL,
	[DictionayCode] [nvarchar](10) NULL,
	[GroupName] [nvarchar](100) NULL,
	[GroupCode] [nvarchar](10) NULL,
	[Useable] [bit] NULL,
	[SortIndex] [int] NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Sys_DataDictionay] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_Attachment]    Script Date: 08/02/2018 15:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_Attachment](
	[Id] [nvarchar](50) NOT NULL,
	[IsImg] [bit] NULL,
	[UrlPath] [nvarchar](500) NULL,
	[ThumUrlPath] [nvarchar](500) NULL,
	[TrueName] [nvarchar](500) NULL,
	[ContentType] [nvarchar](500) NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_dbo.Sys_Attachment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Holiday]    Script Date: 08/02/2018 15:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Holiday](
	[Id] [nvarchar](50) NOT NULL,
	[Person] [nvarchar](50) NULL,
	[UserId] [nvarchar](50) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Days] [decimal](18, 2) NULL,
	[Remark] [nvarchar](500) NULL,
	[CreateDate] [datetime] NULL,
	[FlowId] [nvarchar](50) NULL,
 CONSTRAINT [PK_dbo.Holiday] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_Role]    Script Date: 08/02/2018 15:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_Role](
	[Id] [nvarchar](50) NOT NULL,
	[RoleName] [nvarchar](500) NULL,
	[Remark] [nvarchar](500) NULL,
	[CreateDate] [datetime] NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Sys_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_Role] ([Id], [RoleName], [Remark], [CreateDate], [IsDelete]) VALUES (N'067A9FFD-462F-4398-8006-2B5657CBDDE6', N'系统管理员', N'系统管理员', CAST(0x0000A93000FBD7F3 AS DateTime), 0)
INSERT [dbo].[Sys_Role] ([Id], [RoleName], [Remark], [CreateDate], [IsDelete]) VALUES (N'1230C652-B69B-4407-95FC-EBB7EB964265', N'项目经理', N'项目经理', CAST(0x0000A93000FBD7F3 AS DateTime), 0)
INSERT [dbo].[Sys_Role] ([Id], [RoleName], [Remark], [CreateDate], [IsDelete]) VALUES (N'4997DC7A-BE5F-4256-AEF6-6CB321C73243', N'行政', N'行政', CAST(0x0000A93000FBD7F3 AS DateTime), 0)
INSERT [dbo].[Sys_Role] ([Id], [RoleName], [Remark], [CreateDate], [IsDelete]) VALUES (N'61D72D73-ACD4-4EF9-82BA-DD231B7253BC', N'部门主管', N'部门主管', CAST(0x0000A93000FBD7F3 AS DateTime), 0)
INSERT [dbo].[Sys_Role] ([Id], [RoleName], [Remark], [CreateDate], [IsDelete]) VALUES (N'89ABE7D7-75A0-47D6-BA01-333839222119', N'总经理', N'总经理', CAST(0x0000A93000FBD7F3 AS DateTime), 0)
INSERT [dbo].[Sys_Role] ([Id], [RoleName], [Remark], [CreateDate], [IsDelete]) VALUES (N'A5208032-3C5B-4CDC-91E4-33331EFB7A55', N'流程设计', N'流程设计', CAST(0x0000A93000FBD7F3 AS DateTime), 0)
INSERT [dbo].[Sys_Role] ([Id], [RoleName], [Remark], [CreateDate], [IsDelete]) VALUES (N'E8DC5DBF-C753-4D08-BE50-B4E017209E74', N'普通员工', N'普通员工', CAST(0x0000A93000FBD7F3 AS DateTime), 0)
/****** Object:  Table [dbo].[Sys_Page]    Script Date: 08/02/2018 15:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_Page](
	[Id] [nvarchar](50) NOT NULL,
	[ParentId] [nvarchar](50) NULL,
	[PageCode] [nvarchar](500) NULL,
	[PageName] [nvarchar](500) NULL,
	[PagePath] [nvarchar](500) NULL,
	[Description] [nvarchar](500) NULL,
	[IsUsed] [bit] NULL,
	[CssName] [nvarchar](500) NULL,
	[PathCode] [nvarchar](100) NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Sys_Page] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_Page] ([Id], [ParentId], [PageCode], [PageName], [PagePath], [Description], [IsUsed], [CssName], [PathCode], [IsDelete]) VALUES (N'21BFCC9C-CCD1-4CD4-A190-053137F234A3', N'3D794E41-E1EE-4315-AAF1-4A7732169416', N'100.06', N'流程类别管理', N'/WorkFlowCategory/Index', NULL, 1, NULL, N'AAAF', 0)
INSERT [dbo].[Sys_Page] ([Id], [ParentId], [PageCode], [PageName], [PagePath], [Description], [IsUsed], [CssName], [PathCode], [IsDelete]) VALUES (N'2A094905-4B7D-42D8-945F-FC6CE34E8A99', N'3D794E41-E1EE-4315-AAF1-4A7732169416', N'100.03', N'功能管理', N'/Page/Index', NULL, 1, NULL, N'AAAC', 0)
INSERT [dbo].[Sys_Page] ([Id], [ParentId], [PageCode], [PageName], [PagePath], [Description], [IsUsed], [CssName], [PathCode], [IsDelete]) VALUES (N'3D794E41-E1EE-4315-AAF1-4A7732169416', NULL, N'100', N'系统设置', N'#', NULL, 1, N'fa-cog', N'AA', 0)
INSERT [dbo].[Sys_Page] ([Id], [ParentId], [PageCode], [PageName], [PagePath], [Description], [IsUsed], [CssName], [PathCode], [IsDelete]) VALUES (N'4BC3B535-E1CB-4A3E-B1E0-89B7AF981796', N'3D794E41-E1EE-4315-AAF1-4A7732169416', N'100.01', N'角色管理', N'/Role/Index', NULL, 1, NULL, N'AAAA', 0)
INSERT [dbo].[Sys_Page] ([Id], [ParentId], [PageCode], [PageName], [PagePath], [Description], [IsUsed], [CssName], [PathCode], [IsDelete]) VALUES (N'5B39998B-7322-48E3-94F8-AFC16FB79379', N'D2429C03-0692-4DB6-A499-DD827F2A9915', N'200.99', N'与我相关', N'/WorkFlowCategory/HistoryListView', NULL, 1, NULL, N'ABAC', 0)
INSERT [dbo].[Sys_Page] ([Id], [ParentId], [PageCode], [PageName], [PagePath], [Description], [IsUsed], [CssName], [PathCode], [IsDelete]) VALUES (N'66CF5B3B-EF30-4C3E-AE1C-9C8E2FC1658F', N'D2429C03-0692-4DB6-A499-DD827F2A9915', N'200.02', N'请假审核', N'/Holiday/Audit', NULL, 1, NULL, N'ABAB', 0)
INSERT [dbo].[Sys_Page] ([Id], [ParentId], [PageCode], [PageName], [PagePath], [Description], [IsUsed], [CssName], [PathCode], [IsDelete]) VALUES (N'717EF415-189B-4CDD-A2B1-6DE8826D8493', N'3D794E41-E1EE-4315-AAF1-4A7732169416', N'100.04', N'功能授权', N'/Role/SetRights', NULL, 1, NULL, N'AAAD', 0)
INSERT [dbo].[Sys_Page] ([Id], [ParentId], [PageCode], [PageName], [PagePath], [Description], [IsUsed], [CssName], [PathCode], [IsDelete]) VALUES (N'74B762A7-30B9-482F-B164-B658A4BA2271', N'3D794E41-E1EE-4315-AAF1-4A7732169416', N'100.08', N'运行参数设置', N'/KeyValue/Index', NULL, 1, NULL, N'AAAH', 0)
INSERT [dbo].[Sys_Page] ([Id], [ParentId], [PageCode], [PageName], [PagePath], [Description], [IsUsed], [CssName], [PathCode], [IsDelete]) VALUES (N'929D6813-1856-445E-A612-E804812877FC', N'3D794E41-E1EE-4315-AAF1-4A7732169416', N'100.05', N'用户管理', N'/User/Index', NULL, 1, NULL, N'AAAE', 0)
INSERT [dbo].[Sys_Page] ([Id], [ParentId], [PageCode], [PageName], [PagePath], [Description], [IsUsed], [CssName], [PathCode], [IsDelete]) VALUES (N'BA3AF546-24E0-4725-A253-E58595E18447', N'D2429C03-0692-4DB6-A499-DD827F2A9915', N'200.01', N'请假申请', N'/Holiday/Index', NULL, 1, NULL, N'ABAA', 0)
INSERT [dbo].[Sys_Page] ([Id], [ParentId], [PageCode], [PageName], [PagePath], [Description], [IsUsed], [CssName], [PathCode], [IsDelete]) VALUES (N'C4DCE65B-44A2-4384-AE08-5D80A687A1CC', N'3D794E41-E1EE-4315-AAF1-4A7732169416', N'100.02', N'部门管理', N'/Department/Index', NULL, 1, NULL, N'AAAB', 0)
INSERT [dbo].[Sys_Page] ([Id], [ParentId], [PageCode], [PageName], [PagePath], [Description], [IsUsed], [CssName], [PathCode], [IsDelete]) VALUES (N'D2429C03-0692-4DB6-A499-DD827F2A9915', NULL, N'200', N'流程管理', N'#', NULL, 1, N'fa-commenting', N'AB', 0)
INSERT [dbo].[Sys_Page] ([Id], [ParentId], [PageCode], [PageName], [PagePath], [Description], [IsUsed], [CssName], [PathCode], [IsDelete]) VALUES (N'F9A43C22-EBEA-475D-BDE7-81CDCFAA7352', N'3D794E41-E1EE-4315-AAF1-4A7732169416', N'100.07', N'流程设计', N'/FlowDesign/Index', NULL, 1, NULL, N'AAAG', 0)
/****** Object:  Table [dbo].[Sys_ObjectAttachment]    Script Date: 08/02/2018 15:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_ObjectAttachment](
	[Id] [nvarchar](50) NOT NULL,
	[ObjectId] [nvarchar](50) NULL,
	[AttachmentId] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[GroupCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_dbo.Sys_ObjectAttachment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_User]    Script Date: 08/02/2018 15:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_User](
	[Id] [nvarchar](50) NOT NULL,
	[UserName] [nvarchar](500) NULL,
	[UserCode] [nvarchar](100) NULL,
	[PassWord] [nvarchar](500) NULL,
	[TrueName] [nvarchar](500) NULL,
	[Email] [nvarchar](500) NULL,
	[Mobile] [nvarchar](500) NULL,
	[UserStatus] [int] NULL,
	[PasswordExpirationDate] [datetime] NULL,
	[DepartmentId] [nvarchar](50) NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Sys_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_User] ([Id], [UserName], [UserCode], [PassWord], [TrueName], [Email], [Mobile], [UserStatus], [PasswordExpirationDate], [DepartmentId], [IsDelete]) VALUES (N'52906604-5DFB-47A4-AD75-663AB1504163', N'行政', NULL, N'c4ca4238a0b923820dcc509a6f75849b', N'行政', NULL, NULL, 1, CAST(0x0000B77500FBD8DF AS DateTime), N'20CE56AA-0645-41FD-9FF6-51364D0A62F7', 0)
INSERT [dbo].[Sys_User] ([Id], [UserName], [UserCode], [PassWord], [TrueName], [Email], [Mobile], [UserStatus], [PasswordExpirationDate], [DepartmentId], [IsDelete]) VALUES (N'64FAEF75-C62E-4EAE-89E4-D4DDE26CD4D4', N'一部员工', NULL, N'c4ca4238a0b923820dcc509a6f75849b', N'一部员工', NULL, NULL, 1, CAST(0x0000B77500FBD8DA AS DateTime), N'B0DF6F62-7068-4FA6-82B2-597494327937', 0)
INSERT [dbo].[Sys_User] ([Id], [UserName], [UserCode], [PassWord], [TrueName], [Email], [Mobile], [UserStatus], [PasswordExpirationDate], [DepartmentId], [IsDelete]) VALUES (N'97DFDEE3-FA0D-470F-92C0-3FD0EDC5AB46', N'研发部门主管', NULL, N'c4ca4238a0b923820dcc509a6f75849b', N'研发部门主管', NULL, NULL, 1, CAST(0x0000B77500FBD8CF AS DateTime), N'59485231-7B38-4A95-BCDA-721F87A5BFBB', 0)
INSERT [dbo].[Sys_User] ([Id], [UserName], [UserCode], [PassWord], [TrueName], [Email], [Mobile], [UserStatus], [PasswordExpirationDate], [DepartmentId], [IsDelete]) VALUES (N'A22C56A2-78D7-4B61-8609-D3AD98876159', N'总经理', NULL, N'c4ca4238a0b923820dcc509a6f75849b', N'总经理', NULL, NULL, 1, CAST(0x0000B77500FBD8D7 AS DateTime), N'884FC7B8-47A1-44FD-8970-4FF16A655E87', 0)
INSERT [dbo].[Sys_User] ([Id], [UserName], [UserCode], [PassWord], [TrueName], [Email], [Mobile], [UserStatus], [PasswordExpirationDate], [DepartmentId], [IsDelete]) VALUES (N'C9028C6D-1AEB-473C-8105-4CF03C59766B', N'一部项目经理', NULL, N'c4ca4238a0b923820dcc509a6f75849b', N'一部项目经理', NULL, NULL, 1, CAST(0x0000B77500FBD8D2 AS DateTime), N'B0DF6F62-7068-4FA6-82B2-597494327937', 0)
INSERT [dbo].[Sys_User] ([Id], [UserName], [UserCode], [PassWord], [TrueName], [Email], [Mobile], [UserStatus], [PasswordExpirationDate], [DepartmentId], [IsDelete]) VALUES (N'D34BE365-8F07-4FEE-9713-B079EC4AF9A3', N'admin', NULL, N'54ec50c1d51814b4cb9f15af0347192a', N'系统管理员', NULL, NULL, 1, CAST(0x0000B77500FBD8D5 AS DateTime), N'4A7335F8-367E-4C06-82C0-4DB694DEC5E2', 0)
/****** Object:  Table [dbo].[Sys_RolePage]    Script Date: 08/02/2018 15:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_RolePage](
	[RoleId] [nvarchar](50) NOT NULL,
	[PageId] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_dbo.Sys_RolePage] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC,
	[PageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'067A9FFD-462F-4398-8006-2B5657CBDDE6', N'21BFCC9C-CCD1-4CD4-A190-053137F234A3')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'067A9FFD-462F-4398-8006-2B5657CBDDE6', N'2A094905-4B7D-42D8-945F-FC6CE34E8A99')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'067A9FFD-462F-4398-8006-2B5657CBDDE6', N'3D794E41-E1EE-4315-AAF1-4A7732169416')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'1230C652-B69B-4407-95FC-EBB7EB964265', N'3D794E41-E1EE-4315-AAF1-4A7732169416')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'4997DC7A-BE5F-4256-AEF6-6CB321C73243', N'3D794E41-E1EE-4315-AAF1-4A7732169416')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'61D72D73-ACD4-4EF9-82BA-DD231B7253BC', N'3D794E41-E1EE-4315-AAF1-4A7732169416')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'89ABE7D7-75A0-47D6-BA01-333839222119', N'3D794E41-E1EE-4315-AAF1-4A7732169416')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'A5208032-3C5B-4CDC-91E4-33331EFB7A55', N'3D794E41-E1EE-4315-AAF1-4A7732169416')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'E8DC5DBF-C753-4D08-BE50-B4E017209E74', N'3D794E41-E1EE-4315-AAF1-4A7732169416')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'067A9FFD-462F-4398-8006-2B5657CBDDE6', N'4BC3B535-E1CB-4A3E-B1E0-89B7AF981796')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'1230C652-B69B-4407-95FC-EBB7EB964265', N'5B39998B-7322-48E3-94F8-AFC16FB79379')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'4997DC7A-BE5F-4256-AEF6-6CB321C73243', N'5B39998B-7322-48E3-94F8-AFC16FB79379')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'61D72D73-ACD4-4EF9-82BA-DD231B7253BC', N'5B39998B-7322-48E3-94F8-AFC16FB79379')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'89ABE7D7-75A0-47D6-BA01-333839222119', N'5B39998B-7322-48E3-94F8-AFC16FB79379')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'E8DC5DBF-C753-4D08-BE50-B4E017209E74', N'5B39998B-7322-48E3-94F8-AFC16FB79379')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'1230C652-B69B-4407-95FC-EBB7EB964265', N'66CF5B3B-EF30-4C3E-AE1C-9C8E2FC1658F')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'4997DC7A-BE5F-4256-AEF6-6CB321C73243', N'66CF5B3B-EF30-4C3E-AE1C-9C8E2FC1658F')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'61D72D73-ACD4-4EF9-82BA-DD231B7253BC', N'66CF5B3B-EF30-4C3E-AE1C-9C8E2FC1658F')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'89ABE7D7-75A0-47D6-BA01-333839222119', N'66CF5B3B-EF30-4C3E-AE1C-9C8E2FC1658F')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'067A9FFD-462F-4398-8006-2B5657CBDDE6', N'717EF415-189B-4CDD-A2B1-6DE8826D8493')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'067A9FFD-462F-4398-8006-2B5657CBDDE6', N'74B762A7-30B9-482F-B164-B658A4BA2271')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'067A9FFD-462F-4398-8006-2B5657CBDDE6', N'929D6813-1856-445E-A612-E804812877FC')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'E8DC5DBF-C753-4D08-BE50-B4E017209E74', N'BA3AF546-24E0-4725-A253-E58595E18447')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'067A9FFD-462F-4398-8006-2B5657CBDDE6', N'C4DCE65B-44A2-4384-AE08-5D80A687A1CC')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'1230C652-B69B-4407-95FC-EBB7EB964265', N'D2429C03-0692-4DB6-A499-DD827F2A9915')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'4997DC7A-BE5F-4256-AEF6-6CB321C73243', N'D2429C03-0692-4DB6-A499-DD827F2A9915')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'61D72D73-ACD4-4EF9-82BA-DD231B7253BC', N'D2429C03-0692-4DB6-A499-DD827F2A9915')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'89ABE7D7-75A0-47D6-BA01-333839222119', N'D2429C03-0692-4DB6-A499-DD827F2A9915')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'E8DC5DBF-C753-4D08-BE50-B4E017209E74', N'D2429C03-0692-4DB6-A499-DD827F2A9915')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'067A9FFD-462F-4398-8006-2B5657CBDDE6', N'F9A43C22-EBEA-475D-BDE7-81CDCFAA7352')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'1230C652-B69B-4407-95FC-EBB7EB964265', N'F9A43C22-EBEA-475D-BDE7-81CDCFAA7352')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'4997DC7A-BE5F-4256-AEF6-6CB321C73243', N'F9A43C22-EBEA-475D-BDE7-81CDCFAA7352')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'61D72D73-ACD4-4EF9-82BA-DD231B7253BC', N'F9A43C22-EBEA-475D-BDE7-81CDCFAA7352')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'89ABE7D7-75A0-47D6-BA01-333839222119', N'F9A43C22-EBEA-475D-BDE7-81CDCFAA7352')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'A5208032-3C5B-4CDC-91E4-33331EFB7A55', N'F9A43C22-EBEA-475D-BDE7-81CDCFAA7352')
INSERT [dbo].[Sys_RolePage] ([RoleId], [PageId]) VALUES (N'E8DC5DBF-C753-4D08-BE50-B4E017209E74', N'F9A43C22-EBEA-475D-BDE7-81CDCFAA7352')
/****** Object:  Table [dbo].[Sys_UserRole]    Script Date: 08/02/2018 15:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_UserRole](
	[UserId] [nvarchar](50) NOT NULL,
	[RoleId] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_dbo.Sys_UserRole] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_UserRole] ([UserId], [RoleId]) VALUES (N'D34BE365-8F07-4FEE-9713-B079EC4AF9A3', N'067A9FFD-462F-4398-8006-2B5657CBDDE6')
INSERT [dbo].[Sys_UserRole] ([UserId], [RoleId]) VALUES (N'C9028C6D-1AEB-473C-8105-4CF03C59766B', N'1230C652-B69B-4407-95FC-EBB7EB964265')
INSERT [dbo].[Sys_UserRole] ([UserId], [RoleId]) VALUES (N'52906604-5DFB-47A4-AD75-663AB1504163', N'4997DC7A-BE5F-4256-AEF6-6CB321C73243')
INSERT [dbo].[Sys_UserRole] ([UserId], [RoleId]) VALUES (N'97DFDEE3-FA0D-470F-92C0-3FD0EDC5AB46', N'61D72D73-ACD4-4EF9-82BA-DD231B7253BC')
INSERT [dbo].[Sys_UserRole] ([UserId], [RoleId]) VALUES (N'A22C56A2-78D7-4B61-8609-D3AD98876159', N'89ABE7D7-75A0-47D6-BA01-333839222119')
INSERT [dbo].[Sys_UserRole] ([UserId], [RoleId]) VALUES (N'52906604-5DFB-47A4-AD75-663AB1504163', N'A5208032-3C5B-4CDC-91E4-33331EFB7A55')
INSERT [dbo].[Sys_UserRole] ([UserId], [RoleId]) VALUES (N'64FAEF75-C62E-4EAE-89E4-D4DDE26CD4D4', N'A5208032-3C5B-4CDC-91E4-33331EFB7A55')
INSERT [dbo].[Sys_UserRole] ([UserId], [RoleId]) VALUES (N'97DFDEE3-FA0D-470F-92C0-3FD0EDC5AB46', N'A5208032-3C5B-4CDC-91E4-33331EFB7A55')
INSERT [dbo].[Sys_UserRole] ([UserId], [RoleId]) VALUES (N'A22C56A2-78D7-4B61-8609-D3AD98876159', N'A5208032-3C5B-4CDC-91E4-33331EFB7A55')
INSERT [dbo].[Sys_UserRole] ([UserId], [RoleId]) VALUES (N'C9028C6D-1AEB-473C-8105-4CF03C59766B', N'A5208032-3C5B-4CDC-91E4-33331EFB7A55')
INSERT [dbo].[Sys_UserRole] ([UserId], [RoleId]) VALUES (N'64FAEF75-C62E-4EAE-89E4-D4DDE26CD4D4', N'E8DC5DBF-C753-4D08-BE50-B4E017209E74')
/****** Object:  ForeignKey [FK_dbo.Sys_ObjectAttachment_dbo.Sys_Attachment_AttachmentId]    Script Date: 08/02/2018 15:18:06 ******/
ALTER TABLE [dbo].[Sys_ObjectAttachment]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Sys_ObjectAttachment_dbo.Sys_Attachment_AttachmentId] FOREIGN KEY([AttachmentId])
REFERENCES [dbo].[Sys_Attachment] ([Id])
GO
ALTER TABLE [dbo].[Sys_ObjectAttachment] CHECK CONSTRAINT [FK_dbo.Sys_ObjectAttachment_dbo.Sys_Attachment_AttachmentId]
GO
/****** Object:  ForeignKey [FK_dbo.Sys_RolePage_dbo.Sys_Page_PageId]    Script Date: 08/02/2018 15:18:06 ******/
ALTER TABLE [dbo].[Sys_RolePage]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Sys_RolePage_dbo.Sys_Page_PageId] FOREIGN KEY([PageId])
REFERENCES [dbo].[Sys_Page] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sys_RolePage] CHECK CONSTRAINT [FK_dbo.Sys_RolePage_dbo.Sys_Page_PageId]
GO
/****** Object:  ForeignKey [FK_dbo.Sys_RolePage_dbo.Sys_Role_RoleId]    Script Date: 08/02/2018 15:18:06 ******/
ALTER TABLE [dbo].[Sys_RolePage]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Sys_RolePage_dbo.Sys_Role_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Sys_Role] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sys_RolePage] CHECK CONSTRAINT [FK_dbo.Sys_RolePage_dbo.Sys_Role_RoleId]
GO
/****** Object:  ForeignKey [FK_dbo.Sys_User_dbo.Sys_Department_DepartmentId]    Script Date: 08/02/2018 15:18:06 ******/
ALTER TABLE [dbo].[Sys_User]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Sys_User_dbo.Sys_Department_DepartmentId] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Sys_Department] ([Id])
GO
ALTER TABLE [dbo].[Sys_User] CHECK CONSTRAINT [FK_dbo.Sys_User_dbo.Sys_Department_DepartmentId]
GO
/****** Object:  ForeignKey [FK_dbo.Sys_UserRole_dbo.Sys_Role_RoleId]    Script Date: 08/02/2018 15:18:06 ******/
ALTER TABLE [dbo].[Sys_UserRole]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Sys_UserRole_dbo.Sys_Role_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Sys_Role] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sys_UserRole] CHECK CONSTRAINT [FK_dbo.Sys_UserRole_dbo.Sys_Role_RoleId]
GO
/****** Object:  ForeignKey [FK_dbo.Sys_UserRole_dbo.Sys_User_UserId]    Script Date: 08/02/2018 15:18:06 ******/
ALTER TABLE [dbo].[Sys_UserRole]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Sys_UserRole_dbo.Sys_User_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Sys_User] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sys_UserRole] CHECK CONSTRAINT [FK_dbo.Sys_UserRole_dbo.Sys_User_UserId]
GO




CREATE TABLE [dbo].[WorkFlow_Category](
	[Id] [nvarchar](50) NOT NULL,
	[CategoryName] [nvarchar](500) NULL,
	[SortIndx] [int] NULL,
	[ParentId] [nvarchar](50) NULL,
	[PathCode] [nvarchar](200) NULL,
 CONSTRAINT [PK_dbo.WorkFlow_Category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[WorkFlow_Category] ([Id], [CategoryName], [SortIndx], [ParentId], [PathCode]) VALUES (N'2ADE9FDE-FFD8-4536-8E81-AA1BC8C7B2BD', N'所有', 1, NULL, N'AA')
INSERT [dbo].[WorkFlow_Category] ([Id], [CategoryName], [SortIndx], [ParentId], [PathCode]) VALUES (N'547FEDC8-DFA0-475C-BF64-7EE43ED41E35', N'请假申请', 1, N'2ADE9FDE-FFD8-4536-8E81-AA1BC8C7B2BD', N'AAAA')
/****** Object:  Table [dbo].[WorkFlow_Def]    Script Date: 08/02/2018 15:19:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkFlow_Def](
	[Id] [nvarchar](50) NOT NULL,
	[WorkFlowDefKey] [nvarchar](100) NULL,
	[WorkFlowDefName] [nvarchar](100) NULL,
	[CreateDate] [datetime] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[BusinessUrl] [nvarchar](255) NULL,
	[AuditUrl] [nvarchar](255) NULL,
	[CategoryId] [nvarchar](50) NULL,
 CONSTRAINT [PK_dbo.WorkFlow_Def] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[WorkFlow_Def] ([Id], [WorkFlowDefKey], [WorkFlowDefName], [CreateDate], [IsDelete], [BusinessUrl], [AuditUrl], [CategoryId]) VALUES (N'96F5B45A-6C94-4058-A76C-879FE6FFC09B', N'holiday', N'请假申请', CAST(0x0000A93000FC6DE4 AS DateTime), 0, N'/Holiday/WorkFlowAuditDetailView', N'#', N'547FEDC8-DFA0-475C-BF64-7EE43ED41E35')
/****** Object:  Table [dbo].[WorkFlow_Instance]    Script Date: 08/02/2018 15:19:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkFlow_Instance](
	[Id] [nvarchar](50) NOT NULL,
	[WorkFlowDefId] [nvarchar](50) NULL,
	[InstanceName] [nvarchar](2000) NULL,
	[CreateUser] [nvarchar](50) NULL,
	[CreateUserId] [nvarchar](50) NULL,
	[CreateTime] [datetime] NOT NULL,
	[RunStatus] [int] NOT NULL,
	[Version] [int] NOT NULL,
	[ObjectId] [nvarchar](100) NULL,
	[AssociatedUserId] [nvarchar](2000) NULL,
	[OtherParams] [nvarchar](2000) NULL,
 CONSTRAINT [PK_dbo.WorkFlow_Instance] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WorkFlow_Version]    Script Date: 08/02/2018 15:19:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkFlow_Version](
	[Id] [nvarchar](50) NOT NULL,
	[WorkFlowDefId] [nvarchar](50) NULL,
	[WorkFlowDocument] [nvarchar](max) NULL,
	[VersionNumber] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[Remark] [nvarchar](200) NULL,
 CONSTRAINT [PK_dbo.WorkFlow_Version] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[WorkFlow_Version] ([Id], [WorkFlowDefId], [WorkFlowDocument], [VersionNumber], [CreateDate], [Remark]) VALUES (N'9E3E214A-EFEF-41F5-B627-01AA83380B0D', N'96F5B45A-6C94-4058-A76C-879FE6FFC09B', N'<?xml version="1.0"?>
<WorkFlowDocument xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Nodes>
    <TaskDefinition>
      <Id>AFE7D363-5886-46E1-0D4E-B305EF7F85CA</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>开始</NodeDefName>
      <LeftX>71</LeftX>
      <TopX>47</TopX>
      <Width>54</Width>
      <Height>31</Height>
      <NodeType>start</NodeType>
    </TaskDefinition>
    <TaskDefinition>
      <Id>A5B8BC9F-4391-4540-0274-727C005FEFBB</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>项目经理审核</NodeDefName>
      <LeftX>199</LeftX>
      <TopX>47</TopX>
      <Width>79</Width>
      <Height>31</Height>
      <NodeType>task</NodeType>
      <RoleNames>项目经理;</RoleNames>
      <RoleIds>1230C652-B69B-4407-95FC-EBB7EB964265;</RoleIds>
    </TaskDefinition>
    <TaskDefinition>
      <Id>29671CA8-F791-45F3-01C4-7BDA2EE8C9CA</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>结束</NodeDefName>
      <LeftX>678</LeftX>
      <TopX>47</TopX>
      <Width>81</Width>
      <Height>31</Height>
      <NodeType>end</NodeType>
    </TaskDefinition>
    <TaskDefinition>
      <Id>250AD77D-0E34-494B-0BFD-0350966A0097</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>部门主管审核</NodeDefName>
      <LeftX>363</LeftX>
      <TopX>47</TopX>
      <Width>83</Width>
      <Height>31</Height>
      <NodeType>task</NodeType>
      <RoleNames>部门主管;</RoleNames>
      <RoleIds>61D72D73-ACD4-4EF9-82BA-DD231B7253BC;</RoleIds>
    </TaskDefinition>
    <TaskDefinition>
      <Id>4539DB86-DB0B-4633-0BAA-9DC61DFB84B2</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>行政备案</NodeDefName>
      <LeftX>534</LeftX>
      <TopX>47</TopX>
      <Width>79</Width>
      <Height>31</Height>
      <NodeType>task</NodeType>
      <RoleNames>行政;</RoleNames>
      <RoleIds>4997DC7A-BE5F-4256-AEF6-6CB321C73243;</RoleIds>
    </TaskDefinition>
    <TaskDefinition>
      <Id>527F46EA-6DE4-42B8-04AB-63B0C905C14D</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>总经理审核</NodeDefName>
      <LeftX>360</LeftX>
      <TopX>166</TopX>
      <Width>89</Width>
      <Height>32</Height>
      <NodeType>task</NodeType>
      <RoleNames>总经理;</RoleNames>
      <RoleIds>89ABE7D7-75A0-47D6-BA01-333839222119;</RoleIds>
    </TaskDefinition>
  </Nodes>
  <NodeLinks>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>AFE7D363-5886-46E1-0D4E-B305EF7F85CA</SourceId>
      <TargetId>A5B8BC9F-4391-4540-0274-727C005FEFBB</TargetId>
      <LinkId>con_49</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>124</StartLeft>
      <StartTop>60</StartTop>
      <StartPostion>RightMiddle</StartPostion>
      <EndLeft>196</EndLeft>
      <EndTop>60</EndTop>
      <EndPostion>LeftMiddle</EndPostion>
      <Path>M 0.5 0 L 31.5 0 M 30.5 0 L 36.5 0 M 35.5 0 L 41.5 0 M 41 0 L 71 0 </Path>
      <LinkNameLeft>163</LinkNameLeft>
      <LinkNameTop>62</LinkNameTop>
    </TaskLink>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>A5B8BC9F-4391-4540-0274-727C005FEFBB</SourceId>
      <TargetId>250AD77D-0E34-494B-0BFD-0350966A0097</TargetId>
      <LinkId>con_51</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>277</StartLeft>
      <StartTop>60</StartTop>
      <StartPostion>RightMiddle</StartPostion>
      <EndLeft>360</EndLeft>
      <EndTop>60</EndTop>
      <EndPostion>LeftMiddle</EndPostion>
      <Path>M 0.5 0 L 31.5 0 M 30.5 0 L 42 0 M 41 0 L 52.5 0 M 52 0 L 82 0 </Path>
      <LinkNameLeft>321</LinkNameLeft>
      <LinkNameTop>62</LinkNameTop>
    </TaskLink>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>250AD77D-0E34-494B-0BFD-0350966A0097</SourceId>
      <TargetId>4539DB86-DB0B-4633-0BAA-9DC61DFB84B2</TargetId>
      <LinkId>con_53</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>445</StartLeft>
      <StartTop>60</StartTop>
      <StartPostion>RightMiddle</StartPostion>
      <EndLeft>531</EndLeft>
      <EndTop>60</EndTop>
      <EndPostion>LeftMiddle</EndPostion>
      <Path>M 0.5 0 L 31.5 0 M 30.5 0 L 43.5 0 M 42.5 0 L 55.5 0 M 55 0 L 85 0 </Path>
      <LinkName>小于3天</LinkName>
      <Condition>1</Condition>
      <LinkNameLeft>491</LinkNameLeft>
      <LinkNameTop>62</LinkNameTop>
    </TaskLink>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>250AD77D-0E34-494B-0BFD-0350966A0097</SourceId>
      <TargetId>527F46EA-6DE4-42B8-04AB-63B0C905C14D</TargetId>
      <LinkId>con_55</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>403</StartLeft>
      <StartTop>75</StartTop>
      <StartPostion>BottomCenter</StartPostion>
      <EndLeft>403</EndLeft>
      <EndTop>163</EndTop>
      <EndPostion>TopCenter</EndPostion>
      <Path>M 0 0.5 L 0 31.5 M 0 30.5 L 0 44.5 M 0 43.5 L 0 57.5 M 0 57 L 0 87 </Path>
      <LinkName>大于等于3天</LinkName>
      <Condition>2</Condition>
      <LinkNameLeft>405</LinkNameLeft>
      <LinkNameTop>122</LinkNameTop>
    </TaskLink>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>527F46EA-6DE4-42B8-04AB-63B0C905C14D</SourceId>
      <TargetId>4539DB86-DB0B-4633-0BAA-9DC61DFB84B2</TargetId>
      <LinkId>con_57</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>448</StartLeft>
      <StartTop>179</StartTop>
      <StartPostion>RightMiddle</StartPostion>
      <EndLeft>572</EndLeft>
      <EndTop>75</EndTop>
      <EndPostion>BottomCenter</EndPostion>
      <Path>M 0.5 104 L 31.5 104 M 30.5 104 L 124 104 M 123.5 104.5 L 123.5 30.5 M 123.5 31 L 123.5 1 </Path>
      <LinkNameLeft>564</LinkNameLeft>
      <LinkNameTop>182</LinkNameTop>
    </TaskLink>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>4539DB86-DB0B-4633-0BAA-9DC61DFB84B2</SourceId>
      <TargetId>29671CA8-F791-45F3-01C4-7BDA2EE8C9CA</TargetId>
      <LinkId>con_59</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>612</StartLeft>
      <StartTop>60</StartTop>
      <StartPostion>RightMiddle</StartPostion>
      <EndLeft>675</EndLeft>
      <EndTop>60</EndTop>
      <EndPostion>LeftMiddle</EndPostion>
      <Path>M 0.5 0 L 31.5 0 M 30.5 0 L 32 0 M 31 0 L 32.5 0 M 32 0 L 62 0 </Path>
      <LinkNameLeft>646</LinkNameLeft>
      <LinkNameTop>62</LinkNameTop>
    </TaskLink>
  </NodeLinks>
</WorkFlowDocument>', 1, CAST(0x0000A93000FC6DE6 AS DateTime), NULL)
/****** Object:  Table [dbo].[WorkFlow_Task]    Script Date: 08/02/2018 15:19:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkFlow_Task](
	[Id] [nvarchar](50) NOT NULL,
	[InstanceId] [nvarchar](50) NULL,
	[NodeDefId] [nvarchar](50) NULL,
	[DealDate] [datetime] NULL,
	[DealUser] [nvarchar](50) NULL,
	[DealUserId] [nvarchar](50) NULL,
	[DealRemark] [nvarchar](500) NULL,
	[RunStatus] [int] NOT NULL,
	[TargetUserId] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.WorkFlow_Task] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_dbo.WorkFlow_Def_dbo.WorkFlow_Category_CategoryId]    Script Date: 08/02/2018 15:19:44 ******/
ALTER TABLE [dbo].[WorkFlow_Def]  WITH CHECK ADD  CONSTRAINT [FK_dbo.WorkFlow_Def_dbo.WorkFlow_Category_CategoryId] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[WorkFlow_Category] ([Id])
GO
ALTER TABLE [dbo].[WorkFlow_Def] CHECK CONSTRAINT [FK_dbo.WorkFlow_Def_dbo.WorkFlow_Category_CategoryId]
GO
/****** Object:  ForeignKey [FK_dbo.WorkFlow_Instance_dbo.WorkFlow_Def_WorkFlowDefId]    Script Date: 08/02/2018 15:19:44 ******/
ALTER TABLE [dbo].[WorkFlow_Instance]  WITH CHECK ADD  CONSTRAINT [FK_dbo.WorkFlow_Instance_dbo.WorkFlow_Def_WorkFlowDefId] FOREIGN KEY([WorkFlowDefId])
REFERENCES [dbo].[WorkFlow_Def] ([Id])
GO
ALTER TABLE [dbo].[WorkFlow_Instance] CHECK CONSTRAINT [FK_dbo.WorkFlow_Instance_dbo.WorkFlow_Def_WorkFlowDefId]
GO
/****** Object:  ForeignKey [FK_dbo.WorkFlow_Task_dbo.WorkFlow_Instance_InstanceId]    Script Date: 08/02/2018 15:19:44 ******/
ALTER TABLE [dbo].[WorkFlow_Task]  WITH CHECK ADD  CONSTRAINT [FK_dbo.WorkFlow_Task_dbo.WorkFlow_Instance_InstanceId] FOREIGN KEY([InstanceId])
REFERENCES [dbo].[WorkFlow_Instance] ([Id])
GO
ALTER TABLE [dbo].[WorkFlow_Task] CHECK CONSTRAINT [FK_dbo.WorkFlow_Task_dbo.WorkFlow_Instance_InstanceId]
GO
/****** Object:  ForeignKey [FK_dbo.WorkFlow_Version_dbo.WorkFlow_Def_WorkFlowDefId]    Script Date: 08/02/2018 15:19:44 ******/
ALTER TABLE [dbo].[WorkFlow_Version]  WITH CHECK ADD  CONSTRAINT [FK_dbo.WorkFlow_Version_dbo.WorkFlow_Def_WorkFlowDefId] FOREIGN KEY([WorkFlowDefId])
REFERENCES [dbo].[WorkFlow_Def] ([Id])
GO
ALTER TABLE [dbo].[WorkFlow_Version] CHECK CONSTRAINT [FK_dbo.WorkFlow_Version_dbo.WorkFlow_Def_WorkFlowDefId]
GO
