/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : workflow

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2018-08-02 15:25:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for holiday
-- ----------------------------
DROP TABLE IF EXISTS `holiday`;
CREATE TABLE `holiday` (
  `Id` varchar(50) NOT NULL,
  `Person` varchar(50) DEFAULT NULL,
  `UserId` varchar(50) DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `Days` decimal(18,2) DEFAULT NULL,
  `Remark` varchar(500) DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `FlowId` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of holiday
-- ----------------------------

-- ----------------------------
-- Table structure for sys_attachment
-- ----------------------------
DROP TABLE IF EXISTS `sys_attachment`;
CREATE TABLE `sys_attachment` (
  `Id` varchar(50) NOT NULL,
  `IsImg` tinyint(1) DEFAULT NULL,
  `UrlPath` varchar(500) DEFAULT NULL,
  `ThumUrlPath` varchar(500) DEFAULT NULL,
  `TrueName` varchar(500) DEFAULT NULL,
  `ContentType` varchar(500) DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for sys_datadictionay
-- ----------------------------
DROP TABLE IF EXISTS `sys_datadictionay`;
CREATE TABLE `sys_datadictionay` (
  `Id` varchar(50) NOT NULL,
  `DictionayName` varchar(100) DEFAULT NULL,
  `DictionayCode` varchar(10) DEFAULT NULL,
  `GroupName` varchar(100) DEFAULT NULL,
  `GroupCode` varchar(10) DEFAULT NULL,
  `Useable` tinyint(1) DEFAULT NULL,
  `SortIndex` int(11) DEFAULT NULL,
  `IsDelete` tinyint(1) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_datadictionay
-- ----------------------------

-- ----------------------------
-- Table structure for sys_department
-- ----------------------------
DROP TABLE IF EXISTS `sys_department`;
CREATE TABLE `sys_department` (
  `Id` varchar(50) NOT NULL,
  `DepartmentName` varchar(500) DEFAULT NULL,
  `DepartmentCode` varchar(500) DEFAULT NULL,
  `ParentId` varchar(50) DEFAULT NULL,
  `Remark` varchar(500) DEFAULT NULL,
  `PathCode` varchar(500) DEFAULT NULL,
  `IsDelete` tinyint(1) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_department
-- ----------------------------
INSERT INTO `sys_department` VALUES ('20CE56AA-0645-41FD-9FF6-51364D0A62F7', '行政部', null, '884FC7B8-47A1-44FD-8970-4FF16A655E87', null, 'AAAAAB', '0');
INSERT INTO `sys_department` VALUES ('4A7335F8-367E-4C06-82C0-4DB694DEC5E2', 'Wikesoft', null, null, null, 'AA', '0');
INSERT INTO `sys_department` VALUES ('59485231-7B38-4A95-BCDA-721F87A5BFBB', '研发部', null, '884FC7B8-47A1-44FD-8970-4FF16A655E87', null, 'AAAAAA', '0');
INSERT INTO `sys_department` VALUES ('884FC7B8-47A1-44FD-8970-4FF16A655E87', '总经理办公室', null, '4A7335F8-367E-4C06-82C0-4DB694DEC5E2', null, 'AAAA', '0');
INSERT INTO `sys_department` VALUES ('B0DF6F62-7068-4FA6-82B2-597494327937', '研发一部', null, '59485231-7B38-4A95-BCDA-721F87A5BFBB', null, 'AAAAAAAA', '0');

-- ----------------------------
-- Table structure for sys_keyvalue
-- ----------------------------
DROP TABLE IF EXISTS `sys_keyvalue`;
CREATE TABLE `sys_keyvalue` (
  `Id` varchar(50) NOT NULL,
  `CnName` varchar(500) DEFAULT NULL,
  `Key` varchar(500) DEFAULT NULL,
  `Value` varchar(500) DEFAULT NULL,
  `Memo` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_keyvalue
-- ----------------------------
INSERT INTO `sys_keyvalue` VALUES ('46AEFF2D-5DD4-4470-97B6-64757042EC59', '系统名称', 'SystemTitle', 'WikeFlow', '系统名称');

-- ----------------------------
-- Table structure for sys_objectattachment
-- ----------------------------
DROP TABLE IF EXISTS `sys_objectattachment`;
CREATE TABLE `sys_objectattachment` (
  `Id` varchar(50) NOT NULL,
  `ObjectId` varchar(50) DEFAULT NULL,
  `AttachmentId` varchar(50) DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `GroupCode` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_AttachmentId` (`AttachmentId`) USING HASH,
  CONSTRAINT `FK_Sys_ObjectAttachment_Sys_Attachment_AttachmentId` FOREIGN KEY (`AttachmentId`) REFERENCES `sys_attachment` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_objectattachment
-- ----------------------------

-- ----------------------------
-- Table structure for sys_page
-- ----------------------------
DROP TABLE IF EXISTS `sys_page`;
CREATE TABLE `sys_page` (
  `Id` varchar(50) NOT NULL,
  `ParentId` varchar(50) DEFAULT NULL,
  `PageCode` varchar(500) DEFAULT NULL,
  `PageName` varchar(500) DEFAULT NULL,
  `PagePath` varchar(500) DEFAULT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `IsUsed` tinyint(1) DEFAULT NULL,
  `CssName` varchar(500) DEFAULT NULL,
  `PathCode` varchar(100) DEFAULT NULL,
  `IsDelete` tinyint(1) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_page
-- ----------------------------
INSERT INTO `sys_page` VALUES ('21BFCC9C-CCD1-4CD4-A190-053137F234A3', '3D794E41-E1EE-4315-AAF1-4A7732169416', '100.06', '流程类别管理', '/WorkFlowCategory/Index', null, '1', null, 'AAAF', '0');
INSERT INTO `sys_page` VALUES ('2A094905-4B7D-42D8-945F-FC6CE34E8A99', '3D794E41-E1EE-4315-AAF1-4A7732169416', '100.03', '功能管理', '/Page/Index', null, '1', null, 'AAAC', '0');
INSERT INTO `sys_page` VALUES ('3D794E41-E1EE-4315-AAF1-4A7732169416', null, '100', '系统设置', '#', null, '1', 'fa-cog', 'AA', '0');
INSERT INTO `sys_page` VALUES ('4BC3B535-E1CB-4A3E-B1E0-89B7AF981796', '3D794E41-E1EE-4315-AAF1-4A7732169416', '100.01', '角色管理', '/Role/Index', null, '1', null, 'AAAA', '0');
INSERT INTO `sys_page` VALUES ('5B39998B-7322-48E3-94F8-AFC16FB79379', 'D2429C03-0692-4DB6-A499-DD827F2A9915', '200.99', '与我相关', '/WorkFlowCategory/HistoryListView', null, '1', null, 'ABAC', '0');
INSERT INTO `sys_page` VALUES ('66CF5B3B-EF30-4C3E-AE1C-9C8E2FC1658F', 'D2429C03-0692-4DB6-A499-DD827F2A9915', '200.02', '请假审核', '/Holiday/Audit', null, '1', null, 'ABAB', '0');
INSERT INTO `sys_page` VALUES ('717EF415-189B-4CDD-A2B1-6DE8826D8493', '3D794E41-E1EE-4315-AAF1-4A7732169416', '100.04', '功能授权', '/Role/SetRights', null, '1', null, 'AAAD', '0');
INSERT INTO `sys_page` VALUES ('74B762A7-30B9-482F-B164-B658A4BA2271', '3D794E41-E1EE-4315-AAF1-4A7732169416', '100.08', '运行参数设置', '/KeyValue/Index', null, '1', null, 'AAAH', '0');
INSERT INTO `sys_page` VALUES ('929D6813-1856-445E-A612-E804812877FC', '3D794E41-E1EE-4315-AAF1-4A7732169416', '100.05', '用户管理', '/User/Index', null, '1', null, 'AAAE', '0');
INSERT INTO `sys_page` VALUES ('BA3AF546-24E0-4725-A253-E58595E18447', 'D2429C03-0692-4DB6-A499-DD827F2A9915', '200.01', '请假申请', '/Holiday/Index', null, '1', null, 'ABAA', '0');
INSERT INTO `sys_page` VALUES ('C4DCE65B-44A2-4384-AE08-5D80A687A1CC', '3D794E41-E1EE-4315-AAF1-4A7732169416', '100.02', '部门管理', '/Department/Index', null, '1', null, 'AAAB', '0');
INSERT INTO `sys_page` VALUES ('D2429C03-0692-4DB6-A499-DD827F2A9915', null, '200', '流程管理', '#', null, '1', 'fa-commenting', 'AB', '0');
INSERT INTO `sys_page` VALUES ('F9A43C22-EBEA-475D-BDE7-81CDCFAA7352', '3D794E41-E1EE-4315-AAF1-4A7732169416', '100.07', '流程设计', '/FlowDesign/Index', null, '1', null, 'AAAG', '0');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `Id` varchar(50) NOT NULL,
  `RoleName` varchar(500) DEFAULT NULL,
  `Remark` varchar(500) DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `IsDelete` tinyint(1) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('067A9FFD-462F-4398-8006-2B5657CBDDE6', '系统管理员', '系统管理员', '2018-08-02 15:25:04', '0');
INSERT INTO `sys_role` VALUES ('1230C652-B69B-4407-95FC-EBB7EB964265', '项目经理', '项目经理', '2018-08-02 15:25:04', '0');
INSERT INTO `sys_role` VALUES ('4997DC7A-BE5F-4256-AEF6-6CB321C73243', '行政', '行政', '2018-08-02 15:25:04', '0');
INSERT INTO `sys_role` VALUES ('61D72D73-ACD4-4EF9-82BA-DD231B7253BC', '部门主管', '部门主管', '2018-08-02 15:25:04', '0');
INSERT INTO `sys_role` VALUES ('89ABE7D7-75A0-47D6-BA01-333839222119', '总经理', '总经理', '2018-08-02 15:25:04', '0');
INSERT INTO `sys_role` VALUES ('A5208032-3C5B-4CDC-91E4-33331EFB7A55', '流程设计', '流程设计', '2018-08-02 15:25:04', '0');
INSERT INTO `sys_role` VALUES ('E8DC5DBF-C753-4D08-BE50-B4E017209E74', '普通员工', '普通员工', '2018-08-02 15:25:04', '0');

-- ----------------------------
-- Table structure for sys_rolepage
-- ----------------------------
DROP TABLE IF EXISTS `sys_rolepage`;
CREATE TABLE `sys_rolepage` (
  `RoleId` varchar(50) NOT NULL,
  `PageId` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleId`,`PageId`),
  KEY `IX_RoleId` (`RoleId`) USING HASH,
  KEY `IX_PageId` (`PageId`) USING HASH,
  CONSTRAINT `FK_Sys_RolePage_Sys_Page_PageId` FOREIGN KEY (`PageId`) REFERENCES `sys_page` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Sys_RolePage_Sys_Role_RoleId` FOREIGN KEY (`RoleId`) REFERENCES `sys_role` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_rolepage
-- ----------------------------
INSERT INTO `sys_rolepage` VALUES ('067A9FFD-462F-4398-8006-2B5657CBDDE6', '21BFCC9C-CCD1-4CD4-A190-053137F234A3');
INSERT INTO `sys_rolepage` VALUES ('067A9FFD-462F-4398-8006-2B5657CBDDE6', '2A094905-4B7D-42D8-945F-FC6CE34E8A99');
INSERT INTO `sys_rolepage` VALUES ('067A9FFD-462F-4398-8006-2B5657CBDDE6', '3D794E41-E1EE-4315-AAF1-4A7732169416');
INSERT INTO `sys_rolepage` VALUES ('067A9FFD-462F-4398-8006-2B5657CBDDE6', '4BC3B535-E1CB-4A3E-B1E0-89B7AF981796');
INSERT INTO `sys_rolepage` VALUES ('067A9FFD-462F-4398-8006-2B5657CBDDE6', '717EF415-189B-4CDD-A2B1-6DE8826D8493');
INSERT INTO `sys_rolepage` VALUES ('067A9FFD-462F-4398-8006-2B5657CBDDE6', '74B762A7-30B9-482F-B164-B658A4BA2271');
INSERT INTO `sys_rolepage` VALUES ('067A9FFD-462F-4398-8006-2B5657CBDDE6', '929D6813-1856-445E-A612-E804812877FC');
INSERT INTO `sys_rolepage` VALUES ('067A9FFD-462F-4398-8006-2B5657CBDDE6', 'C4DCE65B-44A2-4384-AE08-5D80A687A1CC');
INSERT INTO `sys_rolepage` VALUES ('067A9FFD-462F-4398-8006-2B5657CBDDE6', 'F9A43C22-EBEA-475D-BDE7-81CDCFAA7352');
INSERT INTO `sys_rolepage` VALUES ('1230C652-B69B-4407-95FC-EBB7EB964265', '3D794E41-E1EE-4315-AAF1-4A7732169416');
INSERT INTO `sys_rolepage` VALUES ('1230C652-B69B-4407-95FC-EBB7EB964265', '5B39998B-7322-48E3-94F8-AFC16FB79379');
INSERT INTO `sys_rolepage` VALUES ('1230C652-B69B-4407-95FC-EBB7EB964265', '66CF5B3B-EF30-4C3E-AE1C-9C8E2FC1658F');
INSERT INTO `sys_rolepage` VALUES ('1230C652-B69B-4407-95FC-EBB7EB964265', 'D2429C03-0692-4DB6-A499-DD827F2A9915');
INSERT INTO `sys_rolepage` VALUES ('1230C652-B69B-4407-95FC-EBB7EB964265', 'F9A43C22-EBEA-475D-BDE7-81CDCFAA7352');
INSERT INTO `sys_rolepage` VALUES ('4997DC7A-BE5F-4256-AEF6-6CB321C73243', '3D794E41-E1EE-4315-AAF1-4A7732169416');
INSERT INTO `sys_rolepage` VALUES ('4997DC7A-BE5F-4256-AEF6-6CB321C73243', '5B39998B-7322-48E3-94F8-AFC16FB79379');
INSERT INTO `sys_rolepage` VALUES ('4997DC7A-BE5F-4256-AEF6-6CB321C73243', '66CF5B3B-EF30-4C3E-AE1C-9C8E2FC1658F');
INSERT INTO `sys_rolepage` VALUES ('4997DC7A-BE5F-4256-AEF6-6CB321C73243', 'D2429C03-0692-4DB6-A499-DD827F2A9915');
INSERT INTO `sys_rolepage` VALUES ('4997DC7A-BE5F-4256-AEF6-6CB321C73243', 'F9A43C22-EBEA-475D-BDE7-81CDCFAA7352');
INSERT INTO `sys_rolepage` VALUES ('61D72D73-ACD4-4EF9-82BA-DD231B7253BC', '3D794E41-E1EE-4315-AAF1-4A7732169416');
INSERT INTO `sys_rolepage` VALUES ('61D72D73-ACD4-4EF9-82BA-DD231B7253BC', '5B39998B-7322-48E3-94F8-AFC16FB79379');
INSERT INTO `sys_rolepage` VALUES ('61D72D73-ACD4-4EF9-82BA-DD231B7253BC', '66CF5B3B-EF30-4C3E-AE1C-9C8E2FC1658F');
INSERT INTO `sys_rolepage` VALUES ('61D72D73-ACD4-4EF9-82BA-DD231B7253BC', 'D2429C03-0692-4DB6-A499-DD827F2A9915');
INSERT INTO `sys_rolepage` VALUES ('61D72D73-ACD4-4EF9-82BA-DD231B7253BC', 'F9A43C22-EBEA-475D-BDE7-81CDCFAA7352');
INSERT INTO `sys_rolepage` VALUES ('89ABE7D7-75A0-47D6-BA01-333839222119', '3D794E41-E1EE-4315-AAF1-4A7732169416');
INSERT INTO `sys_rolepage` VALUES ('89ABE7D7-75A0-47D6-BA01-333839222119', '5B39998B-7322-48E3-94F8-AFC16FB79379');
INSERT INTO `sys_rolepage` VALUES ('89ABE7D7-75A0-47D6-BA01-333839222119', '66CF5B3B-EF30-4C3E-AE1C-9C8E2FC1658F');
INSERT INTO `sys_rolepage` VALUES ('89ABE7D7-75A0-47D6-BA01-333839222119', 'D2429C03-0692-4DB6-A499-DD827F2A9915');
INSERT INTO `sys_rolepage` VALUES ('89ABE7D7-75A0-47D6-BA01-333839222119', 'F9A43C22-EBEA-475D-BDE7-81CDCFAA7352');
INSERT INTO `sys_rolepage` VALUES ('A5208032-3C5B-4CDC-91E4-33331EFB7A55', '3D794E41-E1EE-4315-AAF1-4A7732169416');
INSERT INTO `sys_rolepage` VALUES ('A5208032-3C5B-4CDC-91E4-33331EFB7A55', 'F9A43C22-EBEA-475D-BDE7-81CDCFAA7352');
INSERT INTO `sys_rolepage` VALUES ('E8DC5DBF-C753-4D08-BE50-B4E017209E74', '3D794E41-E1EE-4315-AAF1-4A7732169416');
INSERT INTO `sys_rolepage` VALUES ('E8DC5DBF-C753-4D08-BE50-B4E017209E74', '5B39998B-7322-48E3-94F8-AFC16FB79379');
INSERT INTO `sys_rolepage` VALUES ('E8DC5DBF-C753-4D08-BE50-B4E017209E74', 'BA3AF546-24E0-4725-A253-E58595E18447');
INSERT INTO `sys_rolepage` VALUES ('E8DC5DBF-C753-4D08-BE50-B4E017209E74', 'D2429C03-0692-4DB6-A499-DD827F2A9915');
INSERT INTO `sys_rolepage` VALUES ('E8DC5DBF-C753-4D08-BE50-B4E017209E74', 'F9A43C22-EBEA-475D-BDE7-81CDCFAA7352');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `Id` varchar(50) NOT NULL,
  `UserName` varchar(500) DEFAULT NULL,
  `UserCode` varchar(100) DEFAULT NULL,
  `PassWord` varchar(500) DEFAULT NULL,
  `TrueName` varchar(500) DEFAULT NULL,
  `Email` varchar(500) DEFAULT NULL,
  `Mobile` varchar(500) DEFAULT NULL,
  `UserStatus` int(11) DEFAULT NULL,
  `PasswordExpirationDate` datetime DEFAULT NULL,
  `DepartmentId` varchar(50) DEFAULT NULL,
  `IsDelete` tinyint(1) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_DepartmentId` (`DepartmentId`) USING HASH,
  CONSTRAINT `FK_Sys_User_Sys_Department_DepartmentId` FOREIGN KEY (`DepartmentId`) REFERENCES `sys_department` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('52906604-5DFB-47A4-AD75-663AB1504163', '行政', null, 'c4ca4238a0b923820dcc509a6f75849b', '行政', null, null, '1', '2028-08-02 15:25:04', '20CE56AA-0645-41FD-9FF6-51364D0A62F7', '0');
INSERT INTO `sys_user` VALUES ('64FAEF75-C62E-4EAE-89E4-D4DDE26CD4D4', '一部员工', null, 'c4ca4238a0b923820dcc509a6f75849b', '一部员工', null, null, '1', '2028-08-02 15:25:04', 'B0DF6F62-7068-4FA6-82B2-597494327937', '0');
INSERT INTO `sys_user` VALUES ('97DFDEE3-FA0D-470F-92C0-3FD0EDC5AB46', '研发部门主管', null, 'c4ca4238a0b923820dcc509a6f75849b', '研发部门主管', null, null, '1', '2028-08-02 15:25:04', '59485231-7B38-4A95-BCDA-721F87A5BFBB', '0');
INSERT INTO `sys_user` VALUES ('A22C56A2-78D7-4B61-8609-D3AD98876159', '总经理', null, 'c4ca4238a0b923820dcc509a6f75849b', '总经理', null, null, '1', '2028-08-02 15:25:04', '884FC7B8-47A1-44FD-8970-4FF16A655E87', '0');
INSERT INTO `sys_user` VALUES ('C9028C6D-1AEB-473C-8105-4CF03C59766B', '一部项目经理', null, 'c4ca4238a0b923820dcc509a6f75849b', '一部项目经理', null, null, '1', '2028-08-02 15:25:04', 'B0DF6F62-7068-4FA6-82B2-597494327937', '0');
INSERT INTO `sys_user` VALUES ('D34BE365-8F07-4FEE-9713-B079EC4AF9A3', 'admin', null, '54ec50c1d51814b4cb9f15af0347192a', '系统管理员', null, null, '1', '2028-08-02 15:25:04', '4A7335F8-367E-4C06-82C0-4DB694DEC5E2', '0');

-- ----------------------------
-- Table structure for sys_userrole
-- ----------------------------
DROP TABLE IF EXISTS `sys_userrole`;
CREATE TABLE `sys_userrole` (
  `UserId` varchar(50) NOT NULL,
  `RoleId` varchar(50) NOT NULL,
  PRIMARY KEY (`UserId`,`RoleId`),
  KEY `IX_UserId` (`UserId`) USING HASH,
  KEY `IX_RoleId` (`RoleId`) USING HASH,
  CONSTRAINT `FK_Sys_UserRole_Sys_Role_RoleId` FOREIGN KEY (`RoleId`) REFERENCES `sys_role` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Sys_UserRole_Sys_User_UserId` FOREIGN KEY (`UserId`) REFERENCES `sys_user` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_userrole
-- ----------------------------
INSERT INTO `sys_userrole` VALUES ('52906604-5DFB-47A4-AD75-663AB1504163', '4997DC7A-BE5F-4256-AEF6-6CB321C73243');
INSERT INTO `sys_userrole` VALUES ('52906604-5DFB-47A4-AD75-663AB1504163', 'A5208032-3C5B-4CDC-91E4-33331EFB7A55');
INSERT INTO `sys_userrole` VALUES ('64FAEF75-C62E-4EAE-89E4-D4DDE26CD4D4', 'A5208032-3C5B-4CDC-91E4-33331EFB7A55');
INSERT INTO `sys_userrole` VALUES ('64FAEF75-C62E-4EAE-89E4-D4DDE26CD4D4', 'E8DC5DBF-C753-4D08-BE50-B4E017209E74');
INSERT INTO `sys_userrole` VALUES ('97DFDEE3-FA0D-470F-92C0-3FD0EDC5AB46', '61D72D73-ACD4-4EF9-82BA-DD231B7253BC');
INSERT INTO `sys_userrole` VALUES ('97DFDEE3-FA0D-470F-92C0-3FD0EDC5AB46', 'A5208032-3C5B-4CDC-91E4-33331EFB7A55');
INSERT INTO `sys_userrole` VALUES ('A22C56A2-78D7-4B61-8609-D3AD98876159', '89ABE7D7-75A0-47D6-BA01-333839222119');
INSERT INTO `sys_userrole` VALUES ('A22C56A2-78D7-4B61-8609-D3AD98876159', 'A5208032-3C5B-4CDC-91E4-33331EFB7A55');
INSERT INTO `sys_userrole` VALUES ('C9028C6D-1AEB-473C-8105-4CF03C59766B', '1230C652-B69B-4407-95FC-EBB7EB964265');
INSERT INTO `sys_userrole` VALUES ('C9028C6D-1AEB-473C-8105-4CF03C59766B', 'A5208032-3C5B-4CDC-91E4-33331EFB7A55');
INSERT INTO `sys_userrole` VALUES ('D34BE365-8F07-4FEE-9713-B079EC4AF9A3', '067A9FFD-462F-4398-8006-2B5657CBDDE6');



/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : workflow1

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2018-08-02 15:27:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for workflow_category
-- ----------------------------
DROP TABLE IF EXISTS `workflow_category`;
CREATE TABLE `workflow_category` (
  `Id` varchar(50) NOT NULL,
  `CategoryName` varchar(500) DEFAULT NULL,
  `SortIndx` int(11) DEFAULT NULL,
  `ParentId` varchar(50) DEFAULT NULL,
  `PathCode` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of workflow_category
-- ----------------------------
INSERT INTO `workflow_category` VALUES ('2ADE9FDE-FFD8-4536-8E81-AA1BC8C7B2BD', '所有', '1', null, 'AA');
INSERT INTO `workflow_category` VALUES ('547FEDC8-DFA0-475C-BF64-7EE43ED41E35', '请假申请', '1', '2ADE9FDE-FFD8-4536-8E81-AA1BC8C7B2BD', 'AAAA');

-- ----------------------------
-- Table structure for workflow_def
-- ----------------------------
DROP TABLE IF EXISTS `workflow_def`;
CREATE TABLE `workflow_def` (
  `Id` varchar(50) NOT NULL,
  `WorkFlowDefKey` varchar(100) DEFAULT NULL,
  `WorkFlowDefName` varchar(100) DEFAULT NULL,
  `CreateDate` datetime NOT NULL,
  `IsDelete` tinyint(1) NOT NULL,
  `BusinessUrl` varchar(255) DEFAULT NULL,
  `AuditUrl` varchar(255) DEFAULT NULL,
  `CategoryId` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_CategoryId` (`CategoryId`) USING HASH,
  CONSTRAINT `FK_WorkFlow_Def_WorkFlow_Category_CategoryId` FOREIGN KEY (`CategoryId`) REFERENCES `workflow_category` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of workflow_def
-- ----------------------------
INSERT INTO `workflow_def` VALUES ('96F5B45A-6C94-4058-A76C-879FE6FFC09B', 'holiday', '请假申请', '2018-08-02 15:26:23', '0', '/Holiday/WorkFlowAuditDetailView', '#', '547FEDC8-DFA0-475C-BF64-7EE43ED41E35');

-- ----------------------------
-- Table structure for workflow_instance
-- ----------------------------
DROP TABLE IF EXISTS `workflow_instance`;
CREATE TABLE `workflow_instance` (
  `Id` varchar(50) NOT NULL,
  `WorkFlowDefId` varchar(50) DEFAULT NULL,
  `InstanceName` varchar(2000) DEFAULT NULL,
  `CreateUser` varchar(50) DEFAULT NULL,
  `CreateUserId` varchar(50) DEFAULT NULL,
  `CreateTime` datetime NOT NULL,
  `RunStatus` int(11) NOT NULL,
  `Version` int(11) NOT NULL,
  `ObjectId` varchar(100) DEFAULT NULL,
  `AssociatedUserId` varchar(2000) DEFAULT NULL,
  `OtherParams` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_WorkFlowDefId` (`WorkFlowDefId`) USING HASH,
  CONSTRAINT `FK_WorkFlow_Instance_WorkFlow_Def_WorkFlowDefId` FOREIGN KEY (`WorkFlowDefId`) REFERENCES `workflow_def` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of workflow_instance
-- ----------------------------

-- ----------------------------
-- Table structure for workflow_task
-- ----------------------------
DROP TABLE IF EXISTS `workflow_task`;
CREATE TABLE `workflow_task` (
  `Id` varchar(50) NOT NULL,
  `InstanceId` varchar(50) DEFAULT NULL,
  `NodeDefId` varchar(50) DEFAULT NULL,
  `DealDate` datetime DEFAULT NULL,
  `DealUser` varchar(50) DEFAULT NULL,
  `DealUserId` varchar(50) DEFAULT NULL,
  `DealRemark` varchar(500) DEFAULT NULL,
  `RunStatus` int(11) NOT NULL,
  `TargetUserId` varchar(8000) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_InstanceId` (`InstanceId`) USING HASH,
  CONSTRAINT `FK_WorkFlow_Task_WorkFlow_Instance_InstanceId` FOREIGN KEY (`InstanceId`) REFERENCES `workflow_instance` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of workflow_task
-- ----------------------------

-- ----------------------------
-- Table structure for workflow_version
-- ----------------------------
DROP TABLE IF EXISTS `workflow_version`;
CREATE TABLE `workflow_version` (
  `Id` varchar(50) NOT NULL,
  `WorkFlowDefId` varchar(50) DEFAULT NULL,
  `WorkFlowDocument` varchar(8000) DEFAULT NULL,
  `VersionNumber` int(11) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `Remark` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_WorkFlowDefId` (`WorkFlowDefId`) USING HASH,
  CONSTRAINT `FK_WorkFlow_Version_WorkFlow_Def_WorkFlowDefId` FOREIGN KEY (`WorkFlowDefId`) REFERENCES `workflow_def` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of workflow_version
-- ----------------------------
INSERT INTO `workflow_version` VALUES ('9E3E214A-EFEF-41F5-B627-01AA83380B0D', '96F5B45A-6C94-4058-A76C-879FE6FFC09B', '<?xml version="1.0"?>
<WorkFlowDocument xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Nodes>
    <TaskDefinition>
      <Id>AFE7D363-5886-46E1-0D4E-B305EF7F85CA</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>开始</NodeDefName>
      <LeftX>71</LeftX>
      <TopX>47</TopX>
      <Width>54</Width>
      <Height>31</Height>
      <NodeType>start</NodeType>
    </TaskDefinition>
    <TaskDefinition>
      <Id>A5B8BC9F-4391-4540-0274-727C005FEFBB</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>项目经理审核</NodeDefName>
      <LeftX>199</LeftX>
      <TopX>47</TopX>
      <Width>79</Width>
      <Height>31</Height>
      <NodeType>task</NodeType>
      <RoleNames>项目经理;</RoleNames>
      <RoleIds>1230C652-B69B-4407-95FC-EBB7EB964265;</RoleIds>
    </TaskDefinition>
    <TaskDefinition>
      <Id>29671CA8-F791-45F3-01C4-7BDA2EE8C9CA</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>结束</NodeDefName>
      <LeftX>678</LeftX>
      <TopX>47</TopX>
      <Width>81</Width>
      <Height>31</Height>
      <NodeType>end</NodeType>
    </TaskDefinition>
    <TaskDefinition>
      <Id>250AD77D-0E34-494B-0BFD-0350966A0097</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>部门主管审核</NodeDefName>
      <LeftX>363</LeftX>
      <TopX>47</TopX>
      <Width>83</Width>
      <Height>31</Height>
      <NodeType>task</NodeType>
      <RoleNames>部门主管;</RoleNames>
      <RoleIds>61D72D73-ACD4-4EF9-82BA-DD231B7253BC;</RoleIds>
    </TaskDefinition>
    <TaskDefinition>
      <Id>4539DB86-DB0B-4633-0BAA-9DC61DFB84B2</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>行政备案</NodeDefName>
      <LeftX>534</LeftX>
      <TopX>47</TopX>
      <Width>79</Width>
      <Height>31</Height>
      <NodeType>task</NodeType>
      <RoleNames>行政;</RoleNames>
      <RoleIds>4997DC7A-BE5F-4256-AEF6-6CB321C73243;</RoleIds>
    </TaskDefinition>
    <TaskDefinition>
      <Id>527F46EA-6DE4-42B8-04AB-63B0C905C14D</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>总经理审核</NodeDefName>
      <LeftX>360</LeftX>
      <TopX>166</TopX>
      <Width>89</Width>
      <Height>32</Height>
      <NodeType>task</NodeType>
      <RoleNames>总经理;</RoleNames>
      <RoleIds>89ABE7D7-75A0-47D6-BA01-333839222119;</RoleIds>
    </TaskDefinition>
  </Nodes>
  <NodeLinks>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>AFE7D363-5886-46E1-0D4E-B305EF7F85CA</SourceId>
      <TargetId>A5B8BC9F-4391-4540-0274-727C005FEFBB</TargetId>
      <LinkId>con_49</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>124</StartLeft>
      <StartTop>60</StartTop>
      <StartPostion>RightMiddle</StartPostion>
      <EndLeft>196</EndLeft>
      <EndTop>60</EndTop>
      <EndPostion>LeftMiddle</EndPostion>
      <Path>M 0.5 0 L 31.5 0 M 30.5 0 L 36.5 0 M 35.5 0 L 41.5 0 M 41 0 L 71 0 </Path>
      <LinkNameLeft>163</LinkNameLeft>
      <LinkNameTop>62</LinkNameTop>
    </TaskLink>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>A5B8BC9F-4391-4540-0274-727C005FEFBB</SourceId>
      <TargetId>250AD77D-0E34-494B-0BFD-0350966A0097</TargetId>
      <LinkId>con_51</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>277</StartLeft>
      <StartTop>60</StartTop>
      <StartPostion>RightMiddle</StartPostion>
      <EndLeft>360</EndLeft>
      <EndTop>60</EndTop>
      <EndPostion>LeftMiddle</EndPostion>
      <Path>M 0.5 0 L 31.5 0 M 30.5 0 L 42 0 M 41 0 L 52.5 0 M 52 0 L 82 0 </Path>
      <LinkNameLeft>321</LinkNameLeft>
      <LinkNameTop>62</LinkNameTop>
    </TaskLink>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>250AD77D-0E34-494B-0BFD-0350966A0097</SourceId>
      <TargetId>4539DB86-DB0B-4633-0BAA-9DC61DFB84B2</TargetId>
      <LinkId>con_53</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>445</StartLeft>
      <StartTop>60</StartTop>
      <StartPostion>RightMiddle</StartPostion>
      <EndLeft>531</EndLeft>
      <EndTop>60</EndTop>
      <EndPostion>LeftMiddle</EndPostion>
      <Path>M 0.5 0 L 31.5 0 M 30.5 0 L 43.5 0 M 42.5 0 L 55.5 0 M 55 0 L 85 0 </Path>
      <LinkName>小于3天</LinkName>
      <Condition>1</Condition>
      <LinkNameLeft>491</LinkNameLeft>
      <LinkNameTop>62</LinkNameTop>
    </TaskLink>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>250AD77D-0E34-494B-0BFD-0350966A0097</SourceId>
      <TargetId>527F46EA-6DE4-42B8-04AB-63B0C905C14D</TargetId>
      <LinkId>con_55</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>403</StartLeft>
      <StartTop>75</StartTop>
      <StartPostion>BottomCenter</StartPostion>
      <EndLeft>403</EndLeft>
      <EndTop>163</EndTop>
      <EndPostion>TopCenter</EndPostion>
      <Path>M 0 0.5 L 0 31.5 M 0 30.5 L 0 44.5 M 0 43.5 L 0 57.5 M 0 57 L 0 87 </Path>
      <LinkName>大于等于3天</LinkName>
      <Condition>2</Condition>
      <LinkNameLeft>405</LinkNameLeft>
      <LinkNameTop>122</LinkNameTop>
    </TaskLink>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>527F46EA-6DE4-42B8-04AB-63B0C905C14D</SourceId>
      <TargetId>4539DB86-DB0B-4633-0BAA-9DC61DFB84B2</TargetId>
      <LinkId>con_57</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>448</StartLeft>
      <StartTop>179</StartTop>
      <StartPostion>RightMiddle</StartPostion>
      <EndLeft>572</EndLeft>
      <EndTop>75</EndTop>
      <EndPostion>BottomCenter</EndPostion>
      <Path>M 0.5 104 L 31.5 104 M 30.5 104 L 124 104 M 123.5 104.5 L 123.5 30.5 M 123.5 31 L 123.5 1 </Path>
      <LinkNameLeft>564</LinkNameLeft>
      <LinkNameTop>182</LinkNameTop>
    </TaskLink>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>4539DB86-DB0B-4633-0BAA-9DC61DFB84B2</SourceId>
      <TargetId>29671CA8-F791-45F3-01C4-7BDA2EE8C9CA</TargetId>
      <LinkId>con_59</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>612</StartLeft>
      <StartTop>60</StartTop>
      <StartPostion>RightMiddle</StartPostion>
      <EndLeft>675</EndLeft>
      <EndTop>60</EndTop>
      <EndPostion>LeftMiddle</EndPostion>
      <Path>M 0.5 0 L 31.5 0 M 30.5 0 L 32 0 M 31 0 L 32.5 0 M 32 0 L 62 0 </Path>
      <LinkNameLeft>646</LinkNameLeft>
      <LinkNameTop>62</LinkNameTop>
    </TaskLink>
  </NodeLinks>
</WorkFlowDocument>', '1', '2018-08-02 15:26:23', null);
