﻿using System;
using WikeSoft.Data.Models.Filters;

namespace WikeSoft.Enterprise.Models.Filters.Sys
{
    /// <summary>
    /// 查询过滤器
    /// </summary>
    public class UserFilter : BaseFilter
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public String UserName { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public String TrueName { get; set; }
        /// <summary>
        /// 部门
        /// </summary>
        public string DepartmentId { get; set; }
    }
}
