﻿using System;
using System.ComponentModel.DataAnnotations;
using WikeSoft.Data.Models;


namespace WikeSoft.Enterprise.Models.Sys
{
    /// <summary>
    /// 数据字典模型
    /// </summary>
    public class DataDictionayModel
    {
        ///<summary>
        /// 主键
        ///</summary>
        public string Id { get; set; } // Id (Primary key)

        ///<summary>
        /// 字典名称
        ///</summary>
        public string DictionayName { get; set; } // DictionayName (length: 100)
        /// <summary>
        /// 字典编码
        /// </summary>
        public string DictionayCode { get; set; } // DictionayCode (length: 10)
        /// <summary>
        /// 分组名称
        /// </summary>
        public string GroupName { get; set; } // GroupName (length: 100)
        /// <summary>
        /// 分组编号
        /// </summary>
        public string GroupCode { get; set; } // GroupCode (length: 10)
        ///<summary>
        /// 是否可用
        ///</summary>
        public bool? Useable { get; set; } // Useable
        ///<summary>
        /// 排序（显示用）
        ///</summary>
        public int? SortIndex { get; set; } // SortIndex
    }

    /// <summary>
    /// 数据字典添加模型
    /// </summary>
    public class DataDictionayAddModel
    {

        ///<summary>
        /// 字典名称
        ///</summary>
        [Display(Name = "字典名称")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(100, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string DictionayName { get; set; } // DictionayName (length: 100)
        /// <summary>
        /// 字典编码
        /// </summary>
        [Display(Name = "字典编码")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(100, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string DictionayCode { get; set; } // DictionayCode (length: 10)
        /// <summary>
        /// 分组名称
        /// </summary>
        [Display(Name = "分组名称")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(100, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string GroupName { get; set; } // GroupName (length: 100)
        /// <summary>
        /// 分组编号
        /// </summary>
        [Display(Name = "分组编号")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(100, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string GroupCode { get; set; } // GroupCode (length: 10)
        ///<summary>
        /// 是否可用
        ///</summary>
        [Display(Name = "是否可用")]
        public bool Useable { get; set; } // Useable
        ///<summary>
        /// 排序（显示用）
        ///</summary>
        [Display(Name = "排序")]
        public int? SortIndex { get; set; } // SortIndex
    }

    /// <summary>
    /// 数据字典添加模型
    /// </summary>
    public class DataDictionayEditModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string Id { get; set; }
        ///<summary>
        /// 字典名称
        ///</summary>
        [Display(Name = "字典名称")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(100, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string DictionayName { get; set; } // DictionayName (length: 100)
        /// <summary>
        /// 字典编码
        /// </summary>
        [Display(Name = "字典编码")]
        public string DictionayCode { get; set; } // DictionayCode (length: 10)
        /// <summary>
        /// 分组名称
        /// </summary>
        [Display(Name = "分组名称")]
        public string GroupName { get; set; } // GroupName (length: 100)
        /// <summary>
        /// 分组编号
        /// </summary>
        [Display(Name = "分组编号")]
        public string GroupCode { get; set; } // GroupCode (length: 10)
        ///<summary>
        /// 是否可用
        ///</summary>
        [Display(Name = "是否可用")]
        public bool Useable { get; set; } // Useable
        ///<summary>
        /// 排序（显示用）
        ///</summary>
        [Display(Name = "排序")]
        public int? SortIndex { get; set; } // SortIndex
    }
}
