﻿using System;
using System.Collections.Generic;
using WikeSoft.Core;
using WikeSoft.Data.Models;
using WikeSoft.Data.Models.Sys;
using WikeSoft.Enterprise.Models;
using WikeSoft.Enterprise.Models.Filters.Sys;
using WikeSoft.Enterprise.Models.Sys;

namespace WikeSoft.Enterprise.Interfaces.Sys
{
    /// <summary>
    /// 系统菜单服务
    /// </summary>
    public interface IPageService
    {
        /// <summary>
        ///  添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Add(PageAddModel model);

        /// <summary>
        ///  编辑
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Edit(PageEditModel model);

        /// <summary>
        ///  删除
        /// </summary>
        /// <param name="id">页面ID</param>
        /// <returns></returns>
        bool Delete(string id);

        /// <summary>
        /// 获取递归后的菜单树
        /// </summary>
        /// <returns></returns>
        IList<NavTreeModel> GetNavTreeModels();

        /// <summary>
        /// 根据父节点的ID，获取子节点
        /// </summary>
        /// <param name="parentId">父节点ID</param>
        /// <param name="isUsed">默认查询已启用的,如果为null，则表示查询包含启用和未启用的页面</param>
        /// <returns></returns>
        IList<ZTreeModel> GetByParentId(string parentId, bool? isUsed = true);

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        PageEditModel Find(string id);

        /// <summary>
        /// 获取ztree的树
        /// </summary>
        /// <returns></returns>
        List<ZTreeModel> GetTrees();

        /// <summary>
        /// 根据用户Id获取用户的页面权限
        /// </summary>
        /// <param name="id">用户Id</param>
        /// <returns></returns>
        IList<PageModel> GetPagesByUserId(string id);

        /// <summary>
        /// 检测用户是否有权限
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <param name="pagePath">页面地址</param>
        /// <returns></returns>
        bool HasRight(string userId, string pagePath);

        /// <summary>
        /// 根据pagePath查询数据
        /// </summary>
        /// <param name="pathcode"></param>
        /// <returns></returns>
        PageModel FindByPagePath(String pathcode);

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        PagedResult<PageModel> Query(PageFilter filter);
    }
}
