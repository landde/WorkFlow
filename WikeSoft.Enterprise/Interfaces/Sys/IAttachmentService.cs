﻿using System;
using System.Collections.Generic;
using WikeSoft.Data.Models;
using WikeSoft.Enterprise.Models.Sys;

namespace WikeSoft.Enterprise.Interfaces.Sys
{
    /// <summary>
    /// 附件接口
    /// </summary>
    public interface IAttachmentService
    {
        /// <summary>
        /// 添加附件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Add(AttachmentAddModel model);

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        AttachmentModel Find(string id);

        /// <summary>
        /// 添加图片
        /// </summary>
        /// <param name="urlPath">图片路径</param>
        /// <param name="thumUrlPath">缩略图</param>
        /// <param name="isimg">是否图片</param>
        /// <param name="truename">真实名称</param>
        /// <param name="contenttype">类型</param>
        /// <returns></returns>
        AttachmentModel InsertAttachment(string urlPath,string thumUrlPath,bool isimg,string truename,string contenttype);
       
        /// <summary>
        /// 得到图片列表
        /// </summary>
        /// <param name="objectId">对象ID</param>
        /// <param name="groupCode">分组码</param>
        /// <returns></returns>
        List<AttachmentModel> GetAttachmentsByObject(string objectId,string groupCode);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(string id);

        /// <summary>
        /// 查询 
        /// </summary>
        /// <returns></returns>
        List<AttachmentModel> GetNotUsedList();
    }
}
