﻿using System;
using System.Collections.Generic;
using WikeSoft.Core;
using WikeSoft.Data.Models;
using WikeSoft.Data.Models.Sys;
using WikeSoft.Enterprise.Models;
using WikeSoft.Enterprise.Models.Filters.Sys;
using WikeSoft.Enterprise.Models.Sys;

namespace WikeSoft.Enterprise.Interfaces.Sys
{
    /// <summary>
    /// 部门
    /// </summary>
    public interface IDepartmentService
    {
        /// <summary>
        /// 根据父节点查询列表
        /// </summary>
        /// <param name="parentId">父节点Id</param>
        /// <returns></returns>
        IList<ZTreeModel> GetByParentId(string parentId);

        /// <summary>
        /// 主键查询
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        DepartmentEditModel Find(string id);
        /// <summary>
        /// 添加部门
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Add(DepartmentAddModel model);
        /// <summary>
        /// 删除部门
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(string id);
        /// <summary>
        /// 修改部门信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Edit(DepartmentEditModel model);
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        PagedResult<DepartmentModel> Query(DepartmentFilter filter);
        /// <summary>
        /// 得到所有的部门信息
        /// </summary>
        /// <returns></returns>
        List<DepartmentModel> GetList();

        /// <summary>
        /// 得到用户的父级部门
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <param name="includeSelf">是否包含自己的部门</param>
        /// <returns></returns>
        List<DepartmentModel> GetParentDepartmentByUserId(string userId, bool includeSelf = true);

        /// <summary>
        /// 得到父级节点
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="includeParent"></param>
        /// <returns></returns>
        string GetDepartmentName(string userId, bool includeParent);
        /// <summary>
        /// 得到当前部门及基子部门
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<DepartmentModel> GetMyDepartmentList(string userId);
    }
}
