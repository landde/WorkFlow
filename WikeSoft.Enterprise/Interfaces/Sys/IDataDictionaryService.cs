﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WikeSoft.Core;
using WikeSoft.Data.Models;
using WikeSoft.Enterprise.Models.Filters.Sys;
using WikeSoft.Enterprise.Models.Sys;

namespace WikeSoft.Enterprise.Interfaces.Sys
{
    /// <summary>
    /// 系统数据字典服务
    /// </summary>
    public interface IDataDictionaryService
    {
        /// <summary>
        ///  添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Add(DataDictionayAddModel model);

        /// <summary>
        ///  编辑
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Edit(DataDictionayEditModel model);

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        DataDictionayEditModel Find(string id);

        /// <summary>
        /// 验证字典名是否重复
        /// </summary>
        /// <param name="dictionaryId">字典id，可以为空，为空表示添加</param>
        /// <param name="dictionaryName">字典名称</param>
        /// <returns></returns>
        bool IsExists(string dictionaryId, string dictionaryName);

        /// <summary>
        /// 根据分组编码获取数据字典
        /// </summary>
        /// <param name="groupCode"></param>
        /// <returns></returns>
        IList<SelectListItem> GetByGroupCode(string groupCode);

        /// <summary>
        /// 根据分组编码获取数据字典
        /// </summary>
        /// <param name="groupCode"></param>
        /// <returns></returns>
        IList<DataDictionayModel> GetListByGroupCode(string groupCode);

            /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        PagedResult<DataDictionayModel> Query(DataDictionaryFilter filters);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        bool Delete(IList<string> ids);
    }
}
