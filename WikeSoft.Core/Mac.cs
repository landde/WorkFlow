﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Core
{
 
    /// <summary>
    /// Mac地址
    /// </summary>
    public class Mac
    {
        /// <summary>
        /// 返回Mac地址
        /// </summary>
        /// <returns></returns>
        public static string GetMacAddressByWmi()
        {
            string mac = "";
            try
            {
                ManagementObjectSearcher query = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = 'TRUE'");
                ManagementObjectCollection queryCollection = query.Get();

                foreach (var o in queryCollection)
                {
                    var mo = (ManagementObject) o;
                    if (mo["IPEnabled"].ToString() == "True")
                    {
                        mac = mo["MacAddress"].ToString();
                        break;
                    }
                }
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch
            {
            }
            return mac;
        }
    }
}
