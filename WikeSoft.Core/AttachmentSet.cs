﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Core
{
 
    /// <summary>
    /// 附件设置
    /// </summary>
    public class AttachmentSet
    {
        /// <summary>
        /// 对象名称
        /// </summary>
        public Guid? ObjectId { get; set; }

        /// <summary>
        /// 是否允许删除
        /// </summary>
        public bool HaveRemove { get; set; }

        /// <summary>
        /// 分组码
        /// </summary>
        public string GroupCode { get; set; }

        /// <summary>
        /// 附件名称主要生成hidden的name属性
        /// </summary>
        public String FiledName { get; set; }
    }
}
